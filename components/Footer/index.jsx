import { Footer } from "./styles";
import Image from "next/image";
import SolanaLogo from "../../public/solana.svg";

const PageFooter = () => {
  return (
    <Footer>
      Built with love on
      <Image src={SolanaLogo} alt="Solana Logo" />
    </Footer>
  );
};

export default PageFooter;
