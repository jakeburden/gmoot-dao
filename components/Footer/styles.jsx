import styled from "@emotion/styled";

export const Footer = styled.footer({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  textAlign: "center",
  padding: "40px 0",
});
