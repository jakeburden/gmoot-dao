import {
  Section,
  Header,
  SubHeader,
  Title,
  Description,
  Features,
  Feature,
} from "./styles";

import Image from "next/image";

import FeautreIcon1 from "../../public/FeatureIcons/01.svg";
import FeautreIcon2 from "../../public/FeatureIcons/02.svg";
import FeautreIcon3 from "../../public/FeatureIcons/03.svg";
import FeautreIcon4 from "../../public/FeatureIcons/04.svg";
import FeautreIcon5 from "../../public/FeatureIcons/05.svg";
import FeautreIcon6 from "../../public/FeatureIcons/08.svg";

const features = [
  {
    icon: FeautreIcon1,
    title: "Robust workflow",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat nibh tristique ipsum.",
  },
  {
    icon: FeautreIcon2,
    title: "Flexibility",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat nibh tristique ipsum.",
  },
  {
    icon: FeautreIcon3,
    title: "User friendly",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat nibh tristique ipsum.",
  },
  {
    icon: FeautreIcon4,
    title: "Multiple layouts",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat nibh tristique ipsum.",
  },
  {
    icon: FeautreIcon5,
    title: "Better components",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat nibh tristique ipsum.",
  },
  {
    icon: FeautreIcon6,
    title: "Well organized",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat nibh tristique ipsum.",
  },
];

const PageFeatures = () => {
  return (
    <Section>
      <Header>Lorem ipsum dolor</Header>
      <SubHeader>
        Lorem ipsum is common placeholder text used to demonstrate the graphic
        elements of a document or visual presentation.
      </SubHeader>
      <Features>
        {features.map(({ title, description, icon }, idx) => {
          return (
            <Feature key={idx}>
              <Image src={icon} alt={`${title} icon`} />
              <Title>{title}</Title>
              <Description>{description}</Description>
            </Feature>
          );
        })}
      </Features>
    </Section>
  );
};

export default PageFeatures;
