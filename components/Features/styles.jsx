import styled from "@emotion/styled";

const breakpoints = [576, 768, 992, 1200];
const mq = breakpoints.map((bp) => `@media (min-width: ${bp}px)`);

export const Section = styled.section({
  padding: "64px 16px",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  margin: "0 auto",
  textAlign: "center",
  [mq[1]]: {
    padding: "12px 165px",
  },
});

export const Header = styled.h3({
  fontSize: "48px",
  fontWeight: "800",
  lineHeight: "64px",
  marginBottom: "8px",
});

export const SubHeader = styled.h4({
  fontSize: "18px",
  fontWeight: "400",
  lineHeight: "32px",
  maxWidth: "540px",
  margin: "0 auto",
  marginBottom: "50px",
});

export const Title = styled.h5({
  fontSize: "24px",
  lineHeight: "32px",
  margin: "8px auto",
  fontWeight: 600,
});

export const Description = styled.p({
  fontSize: "16px",
  fontWeight: 400,
  lineHeight: "26px",
  marginBottom: "64px",
});

export const Features = styled.div({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
});

export const Feature = styled.div({
  flex: "0 0 1",
  maxWidth: "350px",
  width: "100%",
  margin: "0 auto",
  [mq[3]]: {
    flex: "0 0 33.333333%",
  },
});
