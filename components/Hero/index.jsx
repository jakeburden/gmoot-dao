import { Header, SubHeader, Container, Button, ImageContainer } from "./styles";
import Image from "next/image";
import HeroImage from "../../public/hero-image.svg";

const PageHero = () => {
  return (
    <Container>
      <ImageContainer>
        <Image
          src={HeroImage}
          alt="Picture of the gmoots in a circle"
          quality={100}
        />
      </ImageContainer>
      <Header>Welcome to gmoot DAO.</Header>
      <SubHeader>On-chain governance for gmoot.</SubHeader>
      <Button href="https://app.sqds.io/">Get Started</Button>
    </Container>
  );
};

export default PageHero;
