import styled from "@emotion/styled";

const breakpoints = [576, 768, 992, 1200];
const mq = breakpoints.map((bp) => `@media (min-width: ${bp}px)`);

export const Header = styled.h1({
  fontSize: "72px",
  fontWeight: 700,
  background: "linear-gradient(268.97deg, #54FFAB 0%, #C97BFF 100%)",
  backgroundClip: "text",
  textFillColor: "transparent",
  marginBottom: "12px",
  fontFamily:
    '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif',
});

export const SubHeader = styled.h2({
  fontSize: "18px",
  fontWeight: 400,
  lineHeight: "32px",
  maxWidth: "730px",
  width: "100%",
  margin: "0 auto",
  marginBottom: "32px",
});

export const Container = styled.div({
  maxWidth: "920px",
  width: "100%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  margin: "0 auto",
  marginTop: "100px",
  textAlign: "center",
  [mq[1]]: {
    marginTop: "180px",
  },
});

export const Button = styled.a({
  background: "#54AFE2",
  color: "#fff",
  padding: "12px 32px",
  borderRadius: "6px",
  maxWidth: "154px",
  margin: "0 auto",
  textDecoration: "none",
});

export const ImageContainer = styled.div({
  height: "248px",
  width: "248px",
  margin: "0 auto",
});
