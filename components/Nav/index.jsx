import { Nav, NavLeft, NavRight, NavGroup } from "./styles";
import Image from "next/image";
import Logo from "../../public/Logo.svg";
import TwitterLogo from "../../public/Twitter-logo.svg";
import DiscordLogo from "../../public/Discord-Logo.svg";

const PageNav = () => {
  return (
    <Nav>
      <NavLeft>
        <a href="/">
          <Image src={Logo} alt="Gmoot DAO Logo" />
        </a>
      </NavLeft>
      <NavRight>
        <NavGroup>
          <a href="https://discord.gg/MhgyXmMNhV">
            <Image src={DiscordLogo} alt="Discord Logo" />
          </a>

          <a href="https://twitter.com/iamgmoot">
            <Image src={TwitterLogo} alt="Twitter Logo" />
          </a>
        </NavGroup>
      </NavRight>
    </Nav>
  );
};

export default PageNav;
