import styled from "@emotion/styled";

const breakpoints = [576, 768, 992, 1200];
const mq = breakpoints.map((bp) => `@media (min-width: ${bp}px)`);

export const Nav = styled.nav({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  position: "fixed",
  top: "0",
  left: "0",
  right: "0",
  padding: "12px 16px",
  backgroundColor: "#fff",
  zIndex: 1,
  minWidth: "200px",
  maring: "0 auto",
  [mq[1]]: {
    padding: "12px 165px",
  },
});

export const NavLeft = styled.div({
  alignItems: "flex-start",
});

export const NavRight = styled.div({
  alignItems: "flex-end",
});

export const NavGroup = styled.div({
  display: "flex",
  width: "100px",
  justifyContent: "space-between",
  alignItems: "center",
});
