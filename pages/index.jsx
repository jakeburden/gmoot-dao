import Head from "next/head";
import { Global } from "@emotion/react";
import Nav from "../components/Nav";
import Hero from "../components/Hero";
import Features from "../components/Features";
import Footer from "../components/Footer";

const Page = () => {
  return (
    <main>
      <Global
        styles={{
          html: {
            fontFamily: "'Inter', sans-serif",
          },
        }}
      />
      <Head>
        <title>gmoot DAO</title>
        <meta charSet="utf-8" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/site.webmanifest" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700;800&family=Source+Sans+Pro:wght@700&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Nav />

      <Hero />
      {/* <Features /> */}
      {/* <Footer /> */}
    </main>
  );
};

export default Page;
